﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SIM_TP1.Utils;
using SIM_TP1.Entities;
using System.Windows.Forms.DataVisualization.Charting;

namespace SIM_TP1
{
    public partial class FormTpUno : Form
    {
        /// <summary>
        /// Valor por defecto del input 'Semilla'
        /// </summary>
        private static readonly int DEFAULT_SEMILLA = 37;

        /// <summary>
        /// Valor por defecto del input 'Constante Multiplicativa'
        /// </summary>
        private static readonly int DEFAULT_CONSTANTE_MULTIPLICATIVA = 19;

        /// <summary>
        /// Valor por defecto del input 'Constante Independiente'
        /// </summary>
        private static readonly int DEFAULT_CONSTANTE_INDEPENDIENTE = 7;

        /// <summary>
        /// Valor por defecto del input 'Modulo'
        /// </summary>
        private static readonly int DEFAULT_MODULO = 53;

        /// <summary>
        /// Valor por defecto del input 'Tamaño muestra'
        /// </summary>
        private static readonly int DEFAULT_N = 20;

        /// <summary>
        /// Esta lista se 'bindea' con el 'dataSource' de la grilla de numeros aleatorios
        /// </summary>
        private BindingList<FilaRandom> bindingListRandoms = new BindingList<FilaRandom>();

        /// <summary>
        /// Esta lista se 'bindea' con el 'dataSource' de la grilla de intervalos
        /// </summary>
        private BindingList<FilaIntervalo> bindingListIntervalos = new BindingList<FilaIntervalo>();

        /// <summary>
        /// Almacena la constante multiplicadora ingresada por el usuario
        /// </summary>
        private int a;

        /// <summary>
        /// Almacena la constante independiente ingresada por el usuario
        /// </summary>
        private int c;

        /// <summary>
        /// Almacena el modulo ingresado por el usuario
        /// </summary>
        private int m;

        private Random random = new Random();

        public FormTpUno()
        {
            InitializeComponent();
            this.inicializarInputValoresPorDefecto();
            this.inicializarCbMuestra();
            this.inputMuestra.Value = 20;
            this.inicializarIntervalos(10, 0.1m);
            this.inicializarRandoms();
            this.inicializarChiCuadrado();
            this.comboMetodoChiCuadrado.SelectedIndex = 0;
        }

        private void inicializarCbMuestra()
        {
            cbMuestra.Items.Add("Default");
            cbMuestra.Items.Add("Personalizado");
            cbMuestra.SelectedIndex = 0;
        }

        /// <summary>
        /// Setea los valores por defecto de los inputs
        /// </summary>
        private void inicializarInputValoresPorDefecto()
        {
            this.inputSemilla.Maximum = DEFAULT_MODULO;
            this.inputSemilla.Value = DEFAULT_SEMILLA;
            this.inputConstanteIndependiente.Value = DEFAULT_CONSTANTE_INDEPENDIENTE;
            this.inputConstanteMultiplicativa.Value = DEFAULT_CONSTANTE_MULTIPLICATIVA;
            this.inputModulo.Value = DEFAULT_MODULO;
            this.inputMuestra.Value = DEFAULT_N;
        }

        /// <summary>
        /// Agrega intervalos inicializados en 0 en la lista de intervalos, partiendo de 0 para el primer limite inferior,
        /// y bindea esa lista de intervalos con el 'dataSource' de la grilla de intervalos
        /// </summary>
        /// <param name="cantidadIntervalos"></param>
        /// <param name="amplitud"></param>
        private void inicializarIntervalos(int cantidadIntervalos, decimal amplitud)
        {
            this.dataGridViewIntervalos.DataSource = this.bindingListIntervalos;
            for (int i = 0; i < cantidadIntervalos; i++)
            {
                FilaIntervalo intervalo = this.getNuevoIntervalo(i, 0, amplitud);
                this.bindingListIntervalos.Add(intervalo);
            }
        }

        /// <summary>
        /// Devuelve un nuevo intervalo con los porcentajes inicializados en 0,
        /// cuyo limite inferior es el 'indice' dividido la cantidad de intervalos,
        /// y su limite superior es el limite inferior mas la amplitud
        /// </summary>
        /// <param name="indice"></param>
        /// <param name="cantidadIntervalos"></param>
        /// <param name="amplitud"></param>
        /// <returns>Un nuevo intervalo</returns>
        private FilaIntervalo getNuevoIntervalo(int indice, decimal minimo, decimal amplitud)
        {
            FilaIntervalo intervalo = new FilaIntervalo();

            var tupla = Util.getNuevoIntervalo(indice, minimo, amplitud);

            intervalo.limiteInferior = tupla.Item1;
            intervalo.limiteSuperior = tupla.Item2;
            intervalo.porcentajeMixto = 0;
            intervalo.porcentajeMultiplicativo = 0;
            intervalo.porcentajeAditivo = 0;
            intervalo.orden = indice + 1;

            return intervalo;
        }

        /// <summary>
        /// Bindea la lista de numeros aleatorios con el 'dataSource' de la grilla de numeros aleatorios
        /// </summary>
        private void inicializarRandoms()
        {
            this.dataGridViewNumeros.AutoGenerateColumns = false;
            this.dataGridViewNumeros.DataSource = this.bindingListRandoms;
        }

        /// <summary>
        /// Genera la primer serie de numeros aleatorios
        /// </summary>
        /// <param name="semilla">Semilla</param>
        /// <param name="a">Constante multiplicativa</param>
        /// <param name="c">Constante independiente</param>
        /// <param name="m">Modulo</param>
        private void generarPrimerRandom(decimal semilla, int a, int c, int m)
        {
            decimal semillaAditivoAnterior = 0;
            decimal semillaMixto = UtilsRandom.getXiMixto(a, c, m, semilla);
            decimal semillaMultiplicativo = UtilsRandom.getXiMultiplicativo(a, m, semilla);
            decimal semillaAditivo = UtilsRandom.getXiAditivo(m, semilla, semillaAditivoAnterior);

            this.addNewFilaRandom(semillaMixto, semillaMultiplicativo, semillaAditivo, m);
        }

        /// <summary>
        /// Genera la cantidad indicada de series de numeros aleatorios.
        /// Antes de comenzar a generar, desactiva el renderizado de las filas en la grilla,
        /// para que se muestren por pantalla todas las series generadas juntas y no de a una.
        /// </summary>
        /// <param name="cantidad">Cantidad a generar</param>
        /// <param name="a">Constante multiplicativa</param>
        /// <param name="c">Constante independiente</param>
        /// <param name="m">Modulo</param>
        private void generarRandoms(int cantidad, int a, int c, int m)
        {
            this.bindingListRandoms.RaiseListChangedEvents = false;

            this.progressBarGenerandoRandoms.Value = 0;
            this.progressBarGenerandoRandoms.Maximum = cantidad;

            decimal semillaMixto = this.getSemillaMixtoSiguiente(a, c, m);
            decimal semillaMultiplicativo = this.getSemillaMultiplicativoSiguiente(a, m);
            decimal semillaAditivo = this.getSemillaAditivoSiguiente(m);

            for (int i = 0; i < cantidad; i++)
            {
                this.progressBarGenerandoRandoms.PerformStep();

                this.addNewFilaRandom(semillaMixto, semillaMultiplicativo, semillaAditivo, m);

                semillaMixto = this.getSemillaMixtoSiguiente(a, c, m);
                semillaMultiplicativo = this.getSemillaMultiplicativoSiguiente(a, m);
                semillaAditivo = this.getSemillaAditivoSiguiente(m);
            }

            this.bindingListRandoms.RaiseListChangedEvents = true;
            this.bindingListRandoms.ResetBindings();
            this.bindingListIntervalos.ResetBindings();
        }

        /// <summary>
        /// Agrega una nueva fila al listado de numeros aleatorios y recalcula los intervalos
        /// </summary>
        /// <param name="semillaMixto">Semilla para generar el aleatorio mixto</param>
        /// <param name="semillaMultiplicativo">Semilla para generar el aleatorio multiplicativo</param>
        /// <param name="semillaAditivo">Semilla para generar el aleatorio aditivo</param>
        /// <param name="m">Modulo</param>
        private void addNewFilaRandom(decimal semillaMixto, decimal semillaMultiplicativo, decimal semillaAditivo, int m)
        {
            int tiradaAcutal = this.bindingListRandoms.Count + 1;

            FilaRandom fila = this.getNewFilaRandom(semillaMixto, semillaMultiplicativo, semillaAditivo, m, tiradaAcutal);

            foreach (var intervalo in this.bindingListIntervalos)
            {
                this.calcularNuevoPorcentajeIntervalo(intervalo, fila.randomMixto, fila.randomMultiplicativo, fila.randomAditivo, fila.randomLenguaje, tiradaAcutal);
            }

            this.bindingListRandoms.Add(fila);
        }

        /// <summary>
        /// Devuelve una nueva fila de numeros aleatorios
        /// </summary>
        /// <param name="semillaMixto">La semilla mixta</param>
        /// <param name="semillaMultiplicativo">La semilla multiplicativa</param>
        /// <param name="semillaAditivo">La semilla aditiva</param>
        /// <param name="m">El modulo</param>
        /// <param name="orden">El numero de tirada</param>
        /// <returns></returns>
        private FilaRandom getNewFilaRandom(decimal semillaMixto, decimal semillaMultiplicativo, decimal semillaAditivo, int m, int orden)
        {
            decimal randomMixto = UtilsRandom.getRandom(semillaMixto, m);
            decimal randomMultiplicativo = UtilsRandom.getRandom(semillaMultiplicativo, m);
            decimal randomAditivo = UtilsRandom.getRandom(semillaAditivo, m);

            FilaRandom fila = new FilaRandom();

            fila.randomMixto = randomMixto;
            fila.randomMultiplicativo = randomMultiplicativo;
            fila.randomAditivo = randomAditivo;
            fila.randomLenguaje = Util.Redondear4Decimales((decimal)this.random.NextDouble());
            fila.semillaMixto = semillaMixto;
            fila.semillaMultiplicativo = semillaMultiplicativo;
            fila.semillaAditivo = semillaAditivo;
            fila.orden = orden;

            return fila;
        }

        /// <summary>
        /// Dada una serie de numeros aleatorios, calcula el nuevo porcentaje de aparicion para esa serie de numeros aleatorios.
        /// </summary>
        /// <param name="intervalo">El intervalo</param>
        /// <param name="randomMixto">El numero aleatorio mixto</param>
        /// <param name="randomMultiplicativo">El numero aleatorio multiplicativo</param>
        /// <param name="randomAditivo">El numero aleatorio aditivo</param>
        /// <param name="tiradaActual">La serie actual</param>
        private void calcularNuevoPorcentajeIntervalo(FilaIntervalo intervalo, decimal randomMixto, decimal randomMultiplicativo, decimal randomAditivo, decimal randomLenguaje, int tiradaActual)
        {
            decimal sumaMixto = 0;
            decimal sumaMultiplicativo = 0;
            decimal sumaAditivo = 0;
            decimal sumaLenguaje = 0;

            if (intervalo.limiteInferior <= randomMixto && randomMixto < intervalo.limiteSuperior)
            {
                sumaMixto = 1;
            }

            if (intervalo.limiteInferior <= randomMultiplicativo && randomMultiplicativo < intervalo.limiteSuperior)
            {
                sumaMultiplicativo = 1;
            }

            if (intervalo.limiteInferior <= randomAditivo && randomAditivo < intervalo.limiteSuperior)
            {
                sumaAditivo = 1;
            }

            if (intervalo.limiteInferior <= randomLenguaje && randomLenguaje < intervalo.limiteSuperior)
            {
                sumaLenguaje = 1;
            }

            intervalo.porcentajeMixto = ((intervalo.porcentajeMixto * (tiradaActual - 1)) + sumaMixto) / tiradaActual;
            intervalo.porcentajeMultiplicativo = ((intervalo.porcentajeMultiplicativo * (tiradaActual - 1)) + sumaMultiplicativo) / tiradaActual;
            intervalo.porcentajeAditivo = ((intervalo.porcentajeAditivo * (tiradaActual - 1)) + sumaAditivo) / tiradaActual;
            intervalo.porcentajeLenguaje = ((intervalo.porcentajeLenguaje * (tiradaActual - 1)) + sumaLenguaje) / tiradaActual;
        }

        /// <summary>
        /// Devuelve la proxima semilla mixta para generar un numero aleatorio en base al numero aleatorio mixto generado en la ultima serie
        /// </summary>
        /// <param name="a">Constante multiplicativa</param>
        /// <param name="c">Constante independiente</param>
        /// <param name="m">Modulo</param>
        /// <returns>La semilla mixta a utilizar en la proxima serie</returns>
        private decimal getSemillaMixtoSiguiente(int a, int c, int m)
        {
            return UtilsRandom.getXiMixto(a, c, m, this.getSemillaMixtoAnterior(m));
        }

        /// <summary>
        /// Devuelve la ultima semilla mixta generada en base al numero aleatorio mixto generado en la ultima serie
        /// </summary>
        /// <param name="m"></param>
        /// <returns>La semilla mixta utilizada en la ultima serie</returns>
        private decimal getSemillaMixtoAnterior (int m)
        {
            //return this.bindingListRandoms.Last().randomMixto * m;
            return this.bindingListRandoms.Last().semillaMixto;
        }

        /// <summary>
        /// Devuelve la proxima semilla multiplicativa para generar un numero aleatorio en base al numero aleatorio multiplicativo generado en la ultima serie
        /// </summary>
        /// <param name="a">Constante multiplicativa</param>
        /// <param name="m">Modulo</param>
        /// <returns>La semilla multiplicativa a utilizar en la proxima serie</returns>
        private decimal getSemillaMultiplicativoSiguiente(int a, int m)
        {
            return UtilsRandom.getXiMultiplicativo(a, m, this.getSemillaMultiplicativoAnterior(m));
        }

        /// <summary>
        /// Devuelve la ultima semilla multiplicativa generada en base al numero aleatorio multiplicativo generado en la ultima serie
        /// </summary>
        /// <param name="m"></param>
        /// <returns>La semilla multiplicativa utilizada en la ultima serie</returns>
        private decimal getSemillaMultiplicativoAnterior(int m)
        {
            //return this.bindingListRandoms.Last().randomMultiplicativo * m;
            return this.bindingListRandoms.Last().semillaMultiplicativo;
        }

        /// <summary>
        /// Devuelve la proxima semilla aditiva para generar un numero aleatorio en base al numero aleatorio aditivo generado en la ultima serie
        /// </summary>
        /// <param name="m">Modulo</param>
        /// <returns>La semilla aditiva a utilizar en la proxima serie</returns>
        private decimal getSemillaAditivoSiguiente(int m)
        {
            return UtilsRandom.getXiAditivo(m, this.getSemillaAditivoAnterior(m), this.getSemillaAditivoAnteriorAnterior(m));
        }

        /// <summary>
        /// Devuelve la ultima semilla aditiva generada en base al numero aleatorio multiplicativo generado en la ultima serie
        /// </summary>
        /// <param name="m"></param>
        /// <returns>La semilla aditiva utilizada en la ultima serie</returns>
        private decimal getSemillaAditivoAnterior(int m)
        {
            //return this.bindingListRandoms.Last().randomAditivo * m;
            return this.bindingListRandoms.Last().semillaAditivo;
        }

        /// <summary>
        /// Devuelve la anteultima semilla aditiva generada en base al numero aleatorio multiplicativo generado en la anteultima serie,
        /// o 0 si no hay una anteultima serie
        /// </summary>
        /// <param name="m"></param>
        /// <returns>La semilla aditiva utilizada en la anteultima serie, 0 si no hay una anteultima serie</returns>
        private decimal getSemillaAditivoAnteriorAnterior(int m)
        {
            if (this.bindingListRandoms.Count > 1)
            {
                //return this.bindingListRandoms.ElementAt(this.bindingListRandoms.Count - 2).randomAditivo * m;
                return this.bindingListRandoms.ElementAt(this.bindingListRandoms.Count - 2).semillaAditivo;
            }
            return 0;
        }

        /// <summary>
        /// Inicia la generación de numeros aleatorios. Ejecuta las siguientes acciones en el siguiente orden:
        /// 1) Vacia los numeros aleatorios generados
        /// 2) Desactiva los inputs del usuario
        /// 3) Setea las variables desde los inputs del usuario
        /// 4) Genera la primer serie de numeros aleatorios
        /// 5) Genera los siguientes numeros aleatorios hasta 20
        /// 6) Mueve el cursos de la grilla de numeros aleatorios hasta la ultima seria
        /// 7) Habilita los botones de avanzar simulacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGenerar_Click(object sender, EventArgs e)
        {
            this.vaciarRandoms();
            this.enableInputs(false);
            this.cbMuestra.Enabled = false;
            this.inputMuestra.Enabled = false;
            this.setVariablesGlobalesFromInputs();

            this.generarPrimerRandom(inputSemilla.Value, this.a, this.c, this.m);

            if (cbMuestra.SelectedIndex == 0)
            {
                this.generarRandoms(19, this.a, this.c, this.m);
                this.enableAvanzarButtons(true);
            } else
            {
                this.generarRandoms((int)inputMuestra.Value - 1, this.a, this.c, this.m);
                this.enableChiCuadradoButtons(true);
            }

            this.dataGridViewNumeros.FirstDisplayedScrollingRowIndex = this.dataGridViewNumeros.Rows.Count - 1;
        }

        private void enableChiCuadradoButtons(Boolean flag)
        {
            this.inputCantidadIntervalos.Enabled = flag;
            this.buttonAutocalcularIntervalos.Enabled = flag;
            this.comboMetodoChiCuadrado.Enabled = flag;
            this.buttonGenerarChiCuadrado.Enabled = flag;

            if (flag)
            {
                this.inputCantidadIntervalos.Maximum = this.bindingListRandoms.Count;
            }
        }

        /// <summary>
        /// Vacia la lista de numeros aleatorios, llama al garbage collector y resetea los intervalos
        /// </summary>
        private void vaciarRandoms()
        {
            this.bindingListRandoms.Clear();
            
            foreach (var intervalo in this.bindingListIntervalos)
            {
                this.resetIntervalo(intervalo);
            }

            this.bindingListIntervalos.ResetBindings();

            this.random = new Random();

            System.GC.Collect();
        }

        /// <summary>
        /// Resetea un intervalo a 0
        /// </summary>
        /// <param name="intervalo">El intervalo</param>
        private void resetIntervalo(FilaIntervalo intervalo)
        {
            intervalo.porcentajeMixto = 0;
            intervalo.porcentajeMultiplicativo = 0;
            intervalo.porcentajeAditivo = 0;
            intervalo.porcentajeLenguaje = 0;
        }

        /// <summary>
        /// Setea las variables de constantes y modulo en base a los valores de los inputs del usuario
        /// </summary>
        private void setVariablesGlobalesFromInputs()
        {
            this.c = Decimal.ToInt32(inputConstanteIndependiente.Value);
            this.a = Decimal.ToInt32(inputConstanteMultiplicativa.Value);
            this.m = Decimal.ToInt32(inputModulo.Value);
        }

        /// <summary>
        /// Activa o desactiva los inputs del usuario en base a la bandera indicada por parametro
        /// </summary>
        /// <param name="flag"></param>
        private void enableInputs(Boolean flag)
        {
            this.buttonGenerar.Enabled = flag;
            this.inputSemilla.Enabled = flag;
            this.inputConstanteIndependiente.Enabled = flag;
            this.inputConstanteMultiplicativa.Enabled = flag;
            this.inputModulo.Enabled = flag;
        }

        /// <summary>
        /// Activa o desactiva los botones de avanzar simulacion en base a la bandera indicada por parametro
        /// </summary>
        /// <param name="flag"></param>
        private void enableAvanzarButtons(Boolean flag)
        {
            this.buttonAvanzarUno.Enabled = flag;
            this.buttonAvanzarVeinte.Enabled = flag;
            this.buttonAvanzarDiezmil.Enabled = flag;
        }

        /// <summary>
        /// Genera una serie de numeros aleatorios y mueve el cursor de la grilla de numeros aleatorios a la ultima serie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAvanzarUno_Click(object sender, EventArgs e)
        {
            this.generarRandoms(1, this.a, this.c, this.m);

            this.dataGridViewNumeros.FirstDisplayedScrollingRowIndex = this.dataGridViewNumeros.Rows.Count - 1;
        }

        /// <summary>
        /// Genera 20 series de numeros aleatorios y mueve el cursor de la grilla de numeros aleatorios a la ultima serie.
        /// Antes de iniciar, desactiva los botones de avanzar simulacion y los reactiva luego de haber generado las series.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAvanzarVeinte_Click(object sender, EventArgs e)
        {
            this.enableAvanzarButtons(false);

            this.generarRandoms(20, this.a, this.c, this.m);

            this.dataGridViewNumeros.FirstDisplayedScrollingRowIndex = this.dataGridViewNumeros.Rows.Count - 1;

            this.enableAvanzarButtons(true);
        }

        /// <summary>
        /// Genera las series de numeros aleatorios que hagan falta para llegar hasta 10000 y mueve el cursor de la grilla de numeros aleatorios a la ultima serie.
        /// Antes de iniciar, desactiva los botones de avanzar simulacion y los reactiva los inputs del usuario luego de haber generado las series.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAvanzarDiezmil_Click(object sender, EventArgs e)
        {
            this.enableAvanzarButtons(false);

            this.generarRandoms(10000 - this.bindingListRandoms.Count, this.a, this.c, this.m);

            this.dataGridViewNumeros.FirstDisplayedScrollingRowIndex = this.dataGridViewNumeros.Rows.Count - 1;

            this.enableInputs(true);

            this.enableChiCuadradoButtons(true);
        }

        /// <summary>
        /// Devuelve el simulador a su estado inicial, vaciando la lista de randoms
        /// y seteando los valores por defecto en los input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonReset_Click(object sender, EventArgs e)
        {
            this.resetGenerador();
        }

        private void resetGenerador()
        {
            this.enableAvanzarButtons(false);

            this.inicializarInputValoresPorDefecto();

            this.enableInputs(true);

            this.vaciarRandoms();

            this.enableChiCuadradoButtons(false);

            this.vaciarRandomsChiCuadrado();

            this.cbMuestra.Enabled = true;
            this.cbMuestra.SelectedIndex = 0;
            this.inputMuestra.Enabled = false;
            this.inputMuestra.Value = 20;
            this.inputCantidadIntervalos.Value = 15;
            this.inputChiCuadradoCalculado.Value = 0;
            this.inputChiCuadradoTabulado.Value = 0;

            this.progressBarChiCuadrado.Value = 0;
            this.progressBarGenerandoRandoms.Value = 0;
        }

        // CHI CUADRADO

        private static readonly decimal DEFAULT_CANTIDAD_NUMEROS = 200;

        private BindingList<FilaChiCuadrado> bindingListChiCuadrado = new BindingList<FilaChiCuadrado>();

        private void inicializarChiCuadrado()
        {
            this.dataGridViewIntervalosMixtoChicuadrado.AutoGenerateColumns = false;
            this.dataGridViewIntervalosMixtoChicuadrado.DataSource = this.bindingListChiCuadrado;

            int cantidadIntervalosDefault = this.getCantidadIntervalos(DEFAULT_CANTIDAD_NUMEROS);
            this.inputCantidadIntervalos.Value = cantidadIntervalosDefault;
        }

        private void vaciarRandomsChiCuadrado()
        {
            this.buttonMostrarGrafico.Enabled = false;

            this.bindingListChiCuadrado.Clear();

            this.inputChiCuadradoCalculado.Value = 0;

            this.bindingListIntervalos.ResetBindings();

            this.chartChiCuadrado.Series["Frecuencia Observada"].Points.Clear();
            this.chartChiCuadrado.Series["Frecuencia Esperada"].Points.Clear();

            System.GC.Collect();
        }

        private void buttonGenerarChiCuadrado_Click(object sender, EventArgs e)
        {
            this.vaciarRandomsChiCuadrado();

            this.progressBarChiCuadrado.Value = 0;

            decimal cantidadNumeros = this.bindingListRandoms.Count;
            decimal cantidadIntervalos = this.inputCantidadIntervalos.Value;

            this.chartChiCuadrado.Width = Convert.ToInt32(30 * cantidadIntervalos);

            List<decimal> randoms = new List<decimal>();

            switch (this.comboMetodoChiCuadrado.SelectedItem)
            {
                case "Mixto":
                    randoms = this.bindingListRandoms.Select(fila => fila.randomMixto).ToList();
                    break;
                case "Multiplicativo":
                    randoms = this.bindingListRandoms.Select(fila => fila.randomMultiplicativo).ToList();
                    break;
                case "Aditivo":
                    randoms = this.bindingListRandoms.Select(fila => fila.randomAditivo).ToList();
                    break;
                case "Lenguaje":
                    randoms = this.bindingListRandoms.Select(fila => fila.randomLenguaje).ToList();
                    break;
                default:
                    break;
            }

            this.bindingListChiCuadrado.RaiseListChangedEvents = false;

            this.inputNivelSignificancia.Value = 0.05m;
            this.inputGradosLibertad.Value = (int)this.inputCantidadIntervalos.Value - 1;

            this.progressBarChiCuadrado.Maximum = Convert.ToInt32(cantidadIntervalos);

            this.inputChiCuadradoCalculado.Value = UtilsRandom.calcularChiCuadrado(randoms, cantidadIntervalos, this.chartChiCuadrado, this.bindingListChiCuadrado, this.progressBarChiCuadrado);
            this.inputChiCuadradoTabulado.Value = UtilsRandom.getChiTabulado((int)cantidadIntervalos - 1, 0.05m);

            textBoxTestChi.Text = UtilsRandom.getRespuestaFinal(inputChiCuadradoCalculado.Value, inputChiCuadradoTabulado.Value);

            this.bindingListChiCuadrado.RaiseListChangedEvents = true;
            this.bindingListChiCuadrado.ResetBindings();
            this.buttonMostrarGrafico.Enabled = true;
        }

        private void buttonAutocalcularIntervalos_Click(object sender, EventArgs e)
        {
            this.inputCantidadIntervalos.Value = this.getCantidadIntervalos(this.bindingListRandoms.Count);
        }

        private int getCantidadIntervalos(decimal cantidadNumeros)
        {
            int cantidadIntervalos = Convert.ToInt32(Math.Ceiling(Math.Sqrt((double)cantidadNumeros)));
            if (cantidadIntervalos % 2 == 0)
            {
                cantidadIntervalos++;
            }
            return cantidadIntervalos;
        }

        private void cbMuestra_SelectedIndexChanged(object sender, EventArgs e)
        {
            inputMuestra.Value = cbMuestra.SelectedIndex == 1 ? inputMuestra.Value : 20;
            inputMuestra.Enabled = cbMuestra.SelectedIndex == 1 ? true : false;
        }

        private void buttonCerrarGrafico_Click(object sender, EventArgs e)
        {
            this.panelGrafico.Visible = false;
            this.panelGrafico.SendToBack();
        }

        private void buttonMostrarGrafico_Click(object sender, EventArgs e)
        {
            this.panelContenedorGrafico.Width = this.tabControlProgramas.Width - 20;
            this.panelContenedorGrafico.Height = this.tabControlProgramas.Height - 80;
            this.chartChiCuadrado.Height = this.tabControlProgramas.Height - 100;
            this.chartChiCuadrado.ChartAreas[0].AxisX.Title = "Número de intervalo";
            this.chartChiCuadrado.ChartAreas[0].AxisY.Title = "Frecuencia";
            this.panelGrafico.Size = this.tabControlProgramas.Size;
            this.panelGrafico.Location = new Point(0, 0);
            this.panelGrafico.Visible = true;
            this.panelGrafico.BringToFront();
        }

        private void inputModulo_ValueChanged(object sender, EventArgs e)
        {
            this.inputSemilla.Maximum = this.inputModulo.Value;
            if (this.inputSemilla.Value > this.inputModulo.Value)
            {
                this.inputSemilla.Value = this.inputModulo.Value;
            }
        }
    }
}
