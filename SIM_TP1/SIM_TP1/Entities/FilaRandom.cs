﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIM_TP1.Entities
{
    /// <summary>
    /// Clase que representa una fila dentro de la grilla de numeros random
    /// </summary>
    internal class FilaRandom
    {
        public decimal randomMixto { get; set; }
        public decimal randomMultiplicativo { get; set; }
        public decimal randomAditivo { get; set; }
        public decimal randomLenguaje { get; set; }
        public int orden { get; set; }
        public decimal semillaMixto { get; set; }
        public decimal semillaMultiplicativo { get; set; }
        public decimal semillaAditivo { get; set; }
    }
}
