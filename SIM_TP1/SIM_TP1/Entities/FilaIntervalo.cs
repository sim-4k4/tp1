﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIM_TP1.Entities
{
    /// <summary>
    /// Clase que representa una fila dentro de la grilla de intervalos
    /// </summary>
    internal class FilaIntervalo
    {
        public decimal limiteInferior { get; set; }
        public decimal limiteSuperior { get; set; }
        public decimal porcentajeMixto { get; set; }
        public decimal porcentajeMultiplicativo { get; set; }
        public decimal porcentajeAditivo { get; set; }
        public decimal porcentajeLenguaje { get; set; }
        public int orden { get; set; }
    }
}
