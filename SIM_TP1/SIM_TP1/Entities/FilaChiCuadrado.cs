﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIM_TP1.Entities
{

    internal class FilaChiCuadrado
    {
        public decimal limiteInferior { get; set; }
        public decimal limiteSuperior { get; set; }
        public decimal frecuenciaObservada { get; set; }
        public decimal frecuenciaEsperada { get; set; }
        public decimal chiCuadradoCalculado { get; set; }
        public int orden { get; set; }
    }
}
