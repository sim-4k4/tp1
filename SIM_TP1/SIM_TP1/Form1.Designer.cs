﻿namespace SIM_TP1
{
    partial class FormTpUno
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTpUno));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonGenerar = new System.Windows.Forms.Button();
            this.dataGridViewNumeros = new System.Windows.Forms.DataGridView();
            this.labelSemilla = new System.Windows.Forms.Label();
            this.inputSemilla = new System.Windows.Forms.NumericUpDown();
            this.inputConstanteIndependiente = new System.Windows.Forms.NumericUpDown();
            this.labelConstanteIndependiente = new System.Windows.Forms.Label();
            this.inputConstanteMultiplicativa = new System.Windows.Forms.NumericUpDown();
            this.labelConstanteMultiplicativa = new System.Windows.Forms.Label();
            this.inputModulo = new System.Windows.Forms.NumericUpDown();
            this.labelModulo = new System.Windows.Forms.Label();
            this.buttonAvanzarUno = new System.Windows.Forms.Button();
            this.buttonAvanzarVeinte = new System.Windows.Forms.Button();
            this.buttonAvanzarDiezmil = new System.Windows.Forms.Button();
            this.tabControlProgramas = new System.Windows.Forms.TabControl();
            this.tabIntegrantes = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPozzoMarcelo = new System.Windows.Forms.Label();
            this.labelIntegrantes = new System.Windows.Forms.Label();
            this.tabGeneradorAleatorios = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxTestChi = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.inputGradosLibertad = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.inputNivelSignificancia = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.inputChiCuadradoTabulado = new System.Windows.Forms.NumericUpDown();
            this.panelGrafico = new System.Windows.Forms.Panel();
            this.buttonCerrarGrafico = new System.Windows.Forms.Button();
            this.panelContenedorGrafico = new System.Windows.Forms.Panel();
            this.chartChiCuadrado = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelFrecuenciaObservada = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelFrecuenciaEsperada = new System.Windows.Forms.Label();
            this.buttonMostrarGrafico = new System.Windows.Forms.Button();
            this.labelMetodo = new System.Windows.Forms.Label();
            this.comboMetodoChiCuadrado = new System.Windows.Forms.ComboBox();
            this.groupBoxCantidadIntervalos = new System.Windows.Forms.GroupBox();
            this.inputCantidadIntervalos = new System.Windows.Forms.NumericUpDown();
            this.buttonAutocalcularIntervalos = new System.Windows.Forms.Button();
            this.progressBarChiCuadrado = new System.Windows.Forms.ProgressBar();
            this.labelChiCuadradoCalculadoMixto = new System.Windows.Forms.Label();
            this.inputChiCuadradoCalculado = new System.Windows.Forms.NumericUpDown();
            this.dataGridViewIntervalosMixtoChicuadrado = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.frecuenciaObservada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.frecuenciaEsperada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonGenerarChiCuadrado = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.inputMuestra = new System.Windows.Forms.NumericUpDown();
            this.cbMuestra = new System.Windows.Forms.ComboBox();
            this.buttonReset = new System.Windows.Forms.Button();
            this.labelIntervalos = new System.Windows.Forms.Label();
            this.labelNumerosGenerados = new System.Windows.Forms.Label();
            this.dataGridViewIntervalos = new System.Windows.Forms.DataGridView();
            this.progressBarGenerandoRandoms = new System.Windows.Forms.ProgressBar();
            this.intervalo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.limiteInferior = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.limiteSuperior = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentajeMixto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentajeMultiplicativo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentajeAditivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentajeLenguaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orden = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnMixto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnMultiplicativo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aditivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lenguaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNumeros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputSemilla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputConstanteIndependiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputConstanteMultiplicativa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputModulo)).BeginInit();
            this.tabControlProgramas.SuspendLayout();
            this.tabIntegrantes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabGeneradorAleatorios.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputGradosLibertad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputNivelSignificancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputChiCuadradoTabulado)).BeginInit();
            this.panelGrafico.SuspendLayout();
            this.panelContenedorGrafico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartChiCuadrado)).BeginInit();
            this.groupBoxCantidadIntervalos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputCantidadIntervalos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputChiCuadradoCalculado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIntervalosMixtoChicuadrado)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputMuestra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIntervalos)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonGenerar
            // 
            this.buttonGenerar.Location = new System.Drawing.Point(54, 291);
            this.buttonGenerar.Name = "buttonGenerar";
            this.buttonGenerar.Size = new System.Drawing.Size(75, 23);
            this.buttonGenerar.TabIndex = 2;
            this.buttonGenerar.Text = "Generar!!!";
            this.buttonGenerar.UseVisualStyleBackColor = true;
            this.buttonGenerar.Click += new System.EventHandler(this.buttonGenerar_Click);
            // 
            // dataGridViewNumeros
            // 
            this.dataGridViewNumeros.AllowUserToAddRows = false;
            this.dataGridViewNumeros.AllowUserToDeleteRows = false;
            this.dataGridViewNumeros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewNumeros.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.orden,
            this.columnMixto,
            this.columnMultiplicativo,
            this.Aditivo,
            this.lenguaje});
            this.dataGridViewNumeros.Location = new System.Drawing.Point(136, 22);
            this.dataGridViewNumeros.Name = "dataGridViewNumeros";
            this.dataGridViewNumeros.ReadOnly = true;
            this.dataGridViewNumeros.Size = new System.Drawing.Size(345, 661);
            this.dataGridViewNumeros.TabIndex = 3;
            // 
            // labelSemilla
            // 
            this.labelSemilla.AutoSize = true;
            this.labelSemilla.Location = new System.Drawing.Point(6, 104);
            this.labelSemilla.Name = "labelSemilla";
            this.labelSemilla.Size = new System.Drawing.Size(40, 13);
            this.labelSemilla.TabIndex = 4;
            this.labelSemilla.Text = "Semilla";
            // 
            // inputSemilla
            // 
            this.inputSemilla.Location = new System.Drawing.Point(9, 120);
            this.inputSemilla.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.inputSemilla.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputSemilla.Name = "inputSemilla";
            this.inputSemilla.Size = new System.Drawing.Size(120, 20);
            this.inputSemilla.TabIndex = 5;
            this.inputSemilla.Value = new decimal(new int[] {
            37,
            0,
            0,
            0});
            // 
            // inputConstanteIndependiente
            // 
            this.inputConstanteIndependiente.Location = new System.Drawing.Point(9, 159);
            this.inputConstanteIndependiente.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.inputConstanteIndependiente.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputConstanteIndependiente.Name = "inputConstanteIndependiente";
            this.inputConstanteIndependiente.Size = new System.Drawing.Size(120, 20);
            this.inputConstanteIndependiente.TabIndex = 7;
            this.inputConstanteIndependiente.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // labelConstanteIndependiente
            // 
            this.labelConstanteIndependiente.AutoSize = true;
            this.labelConstanteIndependiente.Location = new System.Drawing.Point(6, 143);
            this.labelConstanteIndependiente.Name = "labelConstanteIndependiente";
            this.labelConstanteIndependiente.Size = new System.Drawing.Size(126, 13);
            this.labelConstanteIndependiente.TabIndex = 6;
            this.labelConstanteIndependiente.Text = "Constante Independiente";
            // 
            // inputConstanteMultiplicativa
            // 
            this.inputConstanteMultiplicativa.Location = new System.Drawing.Point(9, 197);
            this.inputConstanteMultiplicativa.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.inputConstanteMultiplicativa.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputConstanteMultiplicativa.Name = "inputConstanteMultiplicativa";
            this.inputConstanteMultiplicativa.Size = new System.Drawing.Size(120, 20);
            this.inputConstanteMultiplicativa.TabIndex = 9;
            this.inputConstanteMultiplicativa.Value = new decimal(new int[] {
            19,
            0,
            0,
            0});
            // 
            // labelConstanteMultiplicativa
            // 
            this.labelConstanteMultiplicativa.AutoSize = true;
            this.labelConstanteMultiplicativa.Location = new System.Drawing.Point(6, 181);
            this.labelConstanteMultiplicativa.Name = "labelConstanteMultiplicativa";
            this.labelConstanteMultiplicativa.Size = new System.Drawing.Size(119, 13);
            this.labelConstanteMultiplicativa.TabIndex = 8;
            this.labelConstanteMultiplicativa.Text = "Constante Multiplicativa";
            // 
            // inputModulo
            // 
            this.inputModulo.Location = new System.Drawing.Point(9, 236);
            this.inputModulo.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.inputModulo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputModulo.Name = "inputModulo";
            this.inputModulo.Size = new System.Drawing.Size(120, 20);
            this.inputModulo.TabIndex = 11;
            this.inputModulo.Value = new decimal(new int[] {
            53,
            0,
            0,
            0});
            this.inputModulo.ValueChanged += new System.EventHandler(this.inputModulo_ValueChanged);
            // 
            // labelModulo
            // 
            this.labelModulo.AutoSize = true;
            this.labelModulo.Location = new System.Drawing.Point(6, 220);
            this.labelModulo.Name = "labelModulo";
            this.labelModulo.Size = new System.Drawing.Size(42, 13);
            this.labelModulo.TabIndex = 10;
            this.labelModulo.Text = "Modulo";
            // 
            // buttonAvanzarUno
            // 
            this.buttonAvanzarUno.Enabled = false;
            this.buttonAvanzarUno.Location = new System.Drawing.Point(13, 320);
            this.buttonAvanzarUno.Name = "buttonAvanzarUno";
            this.buttonAvanzarUno.Size = new System.Drawing.Size(116, 23);
            this.buttonAvanzarUno.TabIndex = 12;
            this.buttonAvanzarUno.Text = "Avanzar de a 1";
            this.buttonAvanzarUno.UseVisualStyleBackColor = true;
            this.buttonAvanzarUno.Click += new System.EventHandler(this.buttonAvanzarUno_Click);
            // 
            // buttonAvanzarVeinte
            // 
            this.buttonAvanzarVeinte.Enabled = false;
            this.buttonAvanzarVeinte.Location = new System.Drawing.Point(13, 349);
            this.buttonAvanzarVeinte.Name = "buttonAvanzarVeinte";
            this.buttonAvanzarVeinte.Size = new System.Drawing.Size(116, 23);
            this.buttonAvanzarVeinte.TabIndex = 13;
            this.buttonAvanzarVeinte.Text = "Avanzar de a 20";
            this.buttonAvanzarVeinte.UseVisualStyleBackColor = true;
            this.buttonAvanzarVeinte.Click += new System.EventHandler(this.buttonAvanzarVeinte_Click);
            // 
            // buttonAvanzarDiezmil
            // 
            this.buttonAvanzarDiezmil.Enabled = false;
            this.buttonAvanzarDiezmil.Location = new System.Drawing.Point(13, 378);
            this.buttonAvanzarDiezmil.Name = "buttonAvanzarDiezmil";
            this.buttonAvanzarDiezmil.Size = new System.Drawing.Size(116, 23);
            this.buttonAvanzarDiezmil.TabIndex = 14;
            this.buttonAvanzarDiezmil.Text = "Avanzar hasta 10000";
            this.buttonAvanzarDiezmil.UseVisualStyleBackColor = true;
            this.buttonAvanzarDiezmil.Click += new System.EventHandler(this.buttonAvanzarDiezmil_Click);
            // 
            // tabControlProgramas
            // 
            this.tabControlProgramas.Controls.Add(this.tabIntegrantes);
            this.tabControlProgramas.Controls.Add(this.tabGeneradorAleatorios);
            this.tabControlProgramas.Location = new System.Drawing.Point(3, 3);
            this.tabControlProgramas.Name = "tabControlProgramas";
            this.tabControlProgramas.SelectedIndex = 0;
            this.tabControlProgramas.Size = new System.Drawing.Size(1355, 725);
            this.tabControlProgramas.TabIndex = 15;
            // 
            // tabIntegrantes
            // 
            this.tabIntegrantes.Controls.Add(this.label10);
            this.tabIntegrantes.Controls.Add(this.label9);
            this.tabIntegrantes.Controls.Add(this.label8);
            this.tabIntegrantes.Controls.Add(this.label7);
            this.tabIntegrantes.Controls.Add(this.pictureBox1);
            this.tabIntegrantes.Controls.Add(this.label5);
            this.tabIntegrantes.Controls.Add(this.label4);
            this.tabIntegrantes.Controls.Add(this.label3);
            this.tabIntegrantes.Controls.Add(this.label2);
            this.tabIntegrantes.Controls.Add(this.label1);
            this.tabIntegrantes.Controls.Add(this.labelPozzoMarcelo);
            this.tabIntegrantes.Controls.Add(this.labelIntegrantes);
            this.tabIntegrantes.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabIntegrantes.Location = new System.Drawing.Point(4, 22);
            this.tabIntegrantes.Name = "tabIntegrantes";
            this.tabIntegrantes.Padding = new System.Windows.Forms.Padding(3);
            this.tabIntegrantes.Size = new System.Drawing.Size(1347, 699);
            this.tabIntegrantes.TabIndex = 2;
            this.tabIntegrantes.Text = "Integrantes";
            this.tabIntegrantes.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.Location = new System.Drawing.Point(491, 337);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(381, 17);
            this.label10.TabIndex = 11;
            this.label10.Text = "Trabajo Práctico Nro 1: Generación de Números Aleatorios";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label9.Location = new System.Drawing.Point(582, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(219, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "CÁTEDRA DE SIMULACIÓN 2022";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(556, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(267, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "FACULTAD REGIONAL CÓRDOBA";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(526, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(326, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "UNIVERSIDAD TECNOLOGICA NACIONAL";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(585, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 210);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(161, 589);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Sciarra, Martin - 69650";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(161, 555);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Juncos, Gabriel - 78739";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(161, 520);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Gomez, Ivan - 54910";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(161, 485);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Barberis, Micaela - 75791";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(161, 451);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Cainero, Pablo - 68757";
            // 
            // labelPozzoMarcelo
            // 
            this.labelPozzoMarcelo.AutoSize = true;
            this.labelPozzoMarcelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelPozzoMarcelo.Location = new System.Drawing.Point(161, 420);
            this.labelPozzoMarcelo.Name = "labelPozzoMarcelo";
            this.labelPozzoMarcelo.Size = new System.Drawing.Size(158, 17);
            this.labelPozzoMarcelo.TabIndex = 1;
            this.labelPozzoMarcelo.Text = "Pozzo, Marcelo - 68095";
            // 
            // labelIntegrantes
            // 
            this.labelIntegrantes.AutoSize = true;
            this.labelIntegrantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelIntegrantes.Location = new System.Drawing.Point(22, 385);
            this.labelIntegrantes.Name = "labelIntegrantes";
            this.labelIntegrantes.Size = new System.Drawing.Size(144, 17);
            this.labelIntegrantes.TabIndex = 0;
            this.labelIntegrantes.Text = "Grupo P - Curso 4K4:";
            // 
            // tabGeneradorAleatorios
            // 
            this.tabGeneradorAleatorios.Controls.Add(this.groupBox2);
            this.tabGeneradorAleatorios.Controls.Add(this.label11);
            this.tabGeneradorAleatorios.Controls.Add(this.inputGradosLibertad);
            this.tabGeneradorAleatorios.Controls.Add(this.label12);
            this.tabGeneradorAleatorios.Controls.Add(this.inputNivelSignificancia);
            this.tabGeneradorAleatorios.Controls.Add(this.label6);
            this.tabGeneradorAleatorios.Controls.Add(this.inputChiCuadradoTabulado);
            this.tabGeneradorAleatorios.Controls.Add(this.panelGrafico);
            this.tabGeneradorAleatorios.Controls.Add(this.buttonMostrarGrafico);
            this.tabGeneradorAleatorios.Controls.Add(this.labelMetodo);
            this.tabGeneradorAleatorios.Controls.Add(this.comboMetodoChiCuadrado);
            this.tabGeneradorAleatorios.Controls.Add(this.groupBoxCantidadIntervalos);
            this.tabGeneradorAleatorios.Controls.Add(this.progressBarChiCuadrado);
            this.tabGeneradorAleatorios.Controls.Add(this.labelChiCuadradoCalculadoMixto);
            this.tabGeneradorAleatorios.Controls.Add(this.inputChiCuadradoCalculado);
            this.tabGeneradorAleatorios.Controls.Add(this.dataGridViewIntervalosMixtoChicuadrado);
            this.tabGeneradorAleatorios.Controls.Add(this.buttonGenerarChiCuadrado);
            this.tabGeneradorAleatorios.Controls.Add(this.groupBox1);
            this.tabGeneradorAleatorios.Controls.Add(this.buttonReset);
            this.tabGeneradorAleatorios.Controls.Add(this.labelIntervalos);
            this.tabGeneradorAleatorios.Controls.Add(this.labelNumerosGenerados);
            this.tabGeneradorAleatorios.Controls.Add(this.dataGridViewIntervalos);
            this.tabGeneradorAleatorios.Controls.Add(this.progressBarGenerandoRandoms);
            this.tabGeneradorAleatorios.Controls.Add(this.dataGridViewNumeros);
            this.tabGeneradorAleatorios.Controls.Add(this.buttonAvanzarDiezmil);
            this.tabGeneradorAleatorios.Controls.Add(this.buttonGenerar);
            this.tabGeneradorAleatorios.Controls.Add(this.buttonAvanzarVeinte);
            this.tabGeneradorAleatorios.Controls.Add(this.labelSemilla);
            this.tabGeneradorAleatorios.Controls.Add(this.buttonAvanzarUno);
            this.tabGeneradorAleatorios.Controls.Add(this.inputSemilla);
            this.tabGeneradorAleatorios.Controls.Add(this.inputModulo);
            this.tabGeneradorAleatorios.Controls.Add(this.labelConstanteIndependiente);
            this.tabGeneradorAleatorios.Controls.Add(this.labelModulo);
            this.tabGeneradorAleatorios.Controls.Add(this.inputConstanteIndependiente);
            this.tabGeneradorAleatorios.Controls.Add(this.inputConstanteMultiplicativa);
            this.tabGeneradorAleatorios.Controls.Add(this.labelConstanteMultiplicativa);
            this.tabGeneradorAleatorios.Location = new System.Drawing.Point(4, 22);
            this.tabGeneradorAleatorios.Name = "tabGeneradorAleatorios";
            this.tabGeneradorAleatorios.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneradorAleatorios.Size = new System.Drawing.Size(1347, 699);
            this.tabGeneradorAleatorios.TabIndex = 0;
            this.tabGeneradorAleatorios.Text = "Generador de Números Pseudoaleatorios";
            this.tabGeneradorAleatorios.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxTestChi);
            this.groupBox2.Location = new System.Drawing.Point(1036, 378);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(301, 96);
            this.groupBox2.TabIndex = 50;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Test Chi Cuadrado";
            // 
            // textBoxTestChi
            // 
            this.textBoxTestChi.Enabled = false;
            this.textBoxTestChi.Location = new System.Drawing.Point(6, 19);
            this.textBoxTestChi.Multiline = true;
            this.textBoxTestChi.Name = "textBoxTestChi";
            this.textBoxTestChi.Size = new System.Drawing.Size(289, 70);
            this.textBoxTestChi.TabIndex = 49;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1033, 280);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 13);
            this.label11.TabIndex = 48;
            this.label11.Text = "Grados de Libertad";
            // 
            // inputGradosLibertad
            // 
            this.inputGradosLibertad.DecimalPlaces = 4;
            this.inputGradosLibertad.Enabled = false;
            this.inputGradosLibertad.Location = new System.Drawing.Point(1036, 296);
            this.inputGradosLibertad.Maximum = new decimal(new int[] {
            268435455,
            1042612833,
            542101086,
            0});
            this.inputGradosLibertad.Name = "inputGradosLibertad";
            this.inputGradosLibertad.ReadOnly = true;
            this.inputGradosLibertad.Size = new System.Drawing.Size(120, 20);
            this.inputGradosLibertad.TabIndex = 47;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1216, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 13);
            this.label12.TabIndex = 46;
            this.label12.Text = "Nivel de Significancia";
            // 
            // inputNivelSignificancia
            // 
            this.inputNivelSignificancia.DecimalPlaces = 4;
            this.inputNivelSignificancia.Enabled = false;
            this.inputNivelSignificancia.Location = new System.Drawing.Point(1216, 297);
            this.inputNivelSignificancia.Maximum = new decimal(new int[] {
            268435455,
            1042612833,
            542101086,
            0});
            this.inputNivelSignificancia.Name = "inputNivelSignificancia";
            this.inputNivelSignificancia.ReadOnly = true;
            this.inputNivelSignificancia.Size = new System.Drawing.Size(120, 20);
            this.inputNivelSignificancia.TabIndex = 45;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1033, 320);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Chi Cuadrado Tabulado";
            // 
            // inputChiCuadradoTabulado
            // 
            this.inputChiCuadradoTabulado.DecimalPlaces = 4;
            this.inputChiCuadradoTabulado.Enabled = false;
            this.inputChiCuadradoTabulado.Location = new System.Drawing.Point(1036, 336);
            this.inputChiCuadradoTabulado.Maximum = new decimal(new int[] {
            268435455,
            1042612833,
            542101086,
            0});
            this.inputChiCuadradoTabulado.Name = "inputChiCuadradoTabulado";
            this.inputChiCuadradoTabulado.ReadOnly = true;
            this.inputChiCuadradoTabulado.Size = new System.Drawing.Size(120, 20);
            this.inputChiCuadradoTabulado.TabIndex = 43;
            // 
            // panelGrafico
            // 
            this.panelGrafico.AutoScroll = true;
            this.panelGrafico.Controls.Add(this.buttonCerrarGrafico);
            this.panelGrafico.Controls.Add(this.panelContenedorGrafico);
            this.panelGrafico.Controls.Add(this.panel3);
            this.panelGrafico.Controls.Add(this.labelFrecuenciaObservada);
            this.panelGrafico.Controls.Add(this.panel2);
            this.panelGrafico.Controls.Add(this.labelFrecuenciaEsperada);
            this.panelGrafico.Location = new System.Drawing.Point(895, 480);
            this.panelGrafico.Name = "panelGrafico";
            this.panelGrafico.Size = new System.Drawing.Size(446, 203);
            this.panelGrafico.TabIndex = 41;
            this.panelGrafico.Visible = false;
            // 
            // buttonCerrarGrafico
            // 
            this.buttonCerrarGrafico.Location = new System.Drawing.Point(308, 3);
            this.buttonCerrarGrafico.Name = "buttonCerrarGrafico";
            this.buttonCerrarGrafico.Size = new System.Drawing.Size(109, 23);
            this.buttonCerrarGrafico.TabIndex = 24;
            this.buttonCerrarGrafico.Text = "He visto suficiente";
            this.buttonCerrarGrafico.UseVisualStyleBackColor = true;
            this.buttonCerrarGrafico.Click += new System.EventHandler(this.buttonCerrarGrafico_Click);
            // 
            // panelContenedorGrafico
            // 
            this.panelContenedorGrafico.AutoScroll = true;
            this.panelContenedorGrafico.Controls.Add(this.chartChiCuadrado);
            this.panelContenedorGrafico.Location = new System.Drawing.Point(8, 32);
            this.panelContenedorGrafico.Name = "panelContenedorGrafico";
            this.panelContenedorGrafico.Size = new System.Drawing.Size(440, 270);
            this.panelContenedorGrafico.TabIndex = 46;
            // 
            // chartChiCuadrado
            // 
            chartArea2.AxisX.Interval = 1D;
            chartArea2.AxisX.MaximumAutoSize = 100F;
            chartArea2.Name = "ChartArea1";
            chartArea2.Position.Auto = false;
            chartArea2.Position.Height = 100F;
            chartArea2.Position.Width = 100F;
            this.chartChiCuadrado.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            legend2.Position.Auto = false;
            legend2.Position.Height = 10.91954F;
            legend2.Position.Width = 39.32039F;
            legend2.Position.X = 10F;
            legend2.Position.Y = 3F;
            this.chartChiCuadrado.Legends.Add(legend2);
            this.chartChiCuadrado.Location = new System.Drawing.Point(8, 19);
            this.chartChiCuadrado.Name = "chartChiCuadrado";
            series3.ChartArea = "ChartArea1";
            series3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            series3.IsVisibleInLegend = false;
            series3.Legend = "Legend1";
            series3.Name = "Frecuencia Esperada";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series3.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series4.ChartArea = "ChartArea1";
            series4.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            series4.IsVisibleInLegend = false;
            series4.Legend = "Legend1";
            series4.Name = "Frecuencia Observada";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series4.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chartChiCuadrado.Series.Add(series3);
            this.chartChiCuadrado.Series.Add(series4);
            this.chartChiCuadrado.Size = new System.Drawing.Size(429, 256);
            this.chartChiCuadrado.TabIndex = 23;
            this.chartChiCuadrado.Text = "Test Chi Cuadrado Mixto";
            title2.Name = "Histograma de Frecuencias";
            this.chartChiCuadrado.Titles.Add(title2);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.panel3.Location = new System.Drawing.Point(274, 8);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(28, 15);
            this.panel3.TabIndex = 45;
            // 
            // labelFrecuenciaObservada
            // 
            this.labelFrecuenciaObservada.AutoSize = true;
            this.labelFrecuenciaObservada.Location = new System.Drawing.Point(153, 8);
            this.labelFrecuenciaObservada.Name = "labelFrecuenciaObservada";
            this.labelFrecuenciaObservada.Size = new System.Drawing.Size(115, 13);
            this.labelFrecuenciaObservada.TabIndex = 43;
            this.labelFrecuenciaObservada.Text = "Frecuencia Observada";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.panel2.Location = new System.Drawing.Point(119, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(28, 15);
            this.panel2.TabIndex = 44;
            // 
            // labelFrecuenciaEsperada
            // 
            this.labelFrecuenciaEsperada.AutoSize = true;
            this.labelFrecuenciaEsperada.Location = new System.Drawing.Point(5, 8);
            this.labelFrecuenciaEsperada.Name = "labelFrecuenciaEsperada";
            this.labelFrecuenciaEsperada.Size = new System.Drawing.Size(108, 13);
            this.labelFrecuenciaEsperada.TabIndex = 42;
            this.labelFrecuenciaEsperada.Text = "Frecuencia Esperada";
            // 
            // buttonMostrarGrafico
            // 
            this.buttonMostrarGrafico.Enabled = false;
            this.buttonMostrarGrafico.Location = new System.Drawing.Point(895, 331);
            this.buttonMostrarGrafico.Name = "buttonMostrarGrafico";
            this.buttonMostrarGrafico.Size = new System.Drawing.Size(106, 41);
            this.buttonMostrarGrafico.TabIndex = 43;
            this.buttonMostrarGrafico.Text = "Mostrar Gráfico";
            this.buttonMostrarGrafico.UseVisualStyleBackColor = true;
            this.buttonMostrarGrafico.Click += new System.EventHandler(this.buttonMostrarGrafico_Click);
            // 
            // labelMetodo
            // 
            this.labelMetodo.AutoSize = true;
            this.labelMetodo.Location = new System.Drawing.Point(691, 280);
            this.labelMetodo.Name = "labelMetodo";
            this.labelMetodo.Size = new System.Drawing.Size(43, 13);
            this.labelMetodo.TabIndex = 42;
            this.labelMetodo.Text = "Método";
            // 
            // comboMetodoChiCuadrado
            // 
            this.comboMetodoChiCuadrado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMetodoChiCuadrado.Enabled = false;
            this.comboMetodoChiCuadrado.FormattingEnabled = true;
            this.comboMetodoChiCuadrado.Items.AddRange(new object[] {
            "Mixto",
            "Multiplicativo",
            "Aditivo",
            "Lenguaje"});
            this.comboMetodoChiCuadrado.Location = new System.Drawing.Point(694, 296);
            this.comboMetodoChiCuadrado.Name = "comboMetodoChiCuadrado";
            this.comboMetodoChiCuadrado.Size = new System.Drawing.Size(106, 21);
            this.comboMetodoChiCuadrado.TabIndex = 2;
            // 
            // groupBoxCantidadIntervalos
            // 
            this.groupBoxCantidadIntervalos.Controls.Add(this.inputCantidadIntervalos);
            this.groupBoxCantidadIntervalos.Controls.Add(this.buttonAutocalcularIntervalos);
            this.groupBoxCantidadIntervalos.Location = new System.Drawing.Point(487, 280);
            this.groupBoxCantidadIntervalos.Name = "groupBoxCantidadIntervalos";
            this.groupBoxCantidadIntervalos.Size = new System.Drawing.Size(201, 44);
            this.groupBoxCantidadIntervalos.TabIndex = 21;
            this.groupBoxCantidadIntervalos.TabStop = false;
            this.groupBoxCantidadIntervalos.Text = "Cantidad de Intervalos";
            // 
            // inputCantidadIntervalos
            // 
            this.inputCantidadIntervalos.Enabled = false;
            this.inputCantidadIntervalos.Location = new System.Drawing.Point(6, 16);
            this.inputCantidadIntervalos.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.inputCantidadIntervalos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputCantidadIntervalos.Name = "inputCantidadIntervalos";
            this.inputCantidadIntervalos.Size = new System.Drawing.Size(104, 20);
            this.inputCantidadIntervalos.TabIndex = 33;
            this.inputCantidadIntervalos.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // buttonAutocalcularIntervalos
            // 
            this.buttonAutocalcularIntervalos.Enabled = false;
            this.buttonAutocalcularIntervalos.Location = new System.Drawing.Point(116, 15);
            this.buttonAutocalcularIntervalos.Name = "buttonAutocalcularIntervalos";
            this.buttonAutocalcularIntervalos.Size = new System.Drawing.Size(76, 23);
            this.buttonAutocalcularIntervalos.TabIndex = 34;
            this.buttonAutocalcularIntervalos.Text = "Autocalcular";
            this.buttonAutocalcularIntervalos.UseVisualStyleBackColor = true;
            this.buttonAutocalcularIntervalos.Click += new System.EventHandler(this.buttonAutocalcularIntervalos_Click);
            // 
            // progressBarChiCuadrado
            // 
            this.progressBarChiCuadrado.Location = new System.Drawing.Point(888, 296);
            this.progressBarChiCuadrado.Maximum = 1;
            this.progressBarChiCuadrado.Name = "progressBarChiCuadrado";
            this.progressBarChiCuadrado.Size = new System.Drawing.Size(116, 21);
            this.progressBarChiCuadrado.Step = 1;
            this.progressBarChiCuadrado.TabIndex = 40;
            // 
            // labelChiCuadradoCalculadoMixto
            // 
            this.labelChiCuadradoCalculadoMixto.AutoSize = true;
            this.labelChiCuadradoCalculadoMixto.Location = new System.Drawing.Point(1216, 319);
            this.labelChiCuadradoCalculadoMixto.Name = "labelChiCuadradoCalculadoMixto";
            this.labelChiCuadradoCalculadoMixto.Size = new System.Drawing.Size(121, 13);
            this.labelChiCuadradoCalculadoMixto.TabIndex = 39;
            this.labelChiCuadradoCalculadoMixto.Text = "Chi Cuadrado Calculado";
            // 
            // inputChiCuadradoCalculado
            // 
            this.inputChiCuadradoCalculado.DecimalPlaces = 4;
            this.inputChiCuadradoCalculado.Enabled = false;
            this.inputChiCuadradoCalculado.Location = new System.Drawing.Point(1216, 337);
            this.inputChiCuadradoCalculado.Maximum = new decimal(new int[] {
            268435455,
            1042612833,
            542101086,
            0});
            this.inputChiCuadradoCalculado.Name = "inputChiCuadradoCalculado";
            this.inputChiCuadradoCalculado.ReadOnly = true;
            this.inputChiCuadradoCalculado.Size = new System.Drawing.Size(120, 20);
            this.inputChiCuadradoCalculado.TabIndex = 38;
            // 
            // dataGridViewIntervalosMixtoChicuadrado
            // 
            this.dataGridViewIntervalosMixtoChicuadrado.AllowUserToAddRows = false;
            this.dataGridViewIntervalosMixtoChicuadrado.AllowUserToDeleteRows = false;
            this.dataGridViewIntervalosMixtoChicuadrado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIntervalosMixtoChicuadrado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.frecuenciaObservada,
            this.frecuenciaEsperada});
            this.dataGridViewIntervalosMixtoChicuadrado.Location = new System.Drawing.Point(487, 331);
            this.dataGridViewIntervalosMixtoChicuadrado.Name = "dataGridViewIntervalosMixtoChicuadrado";
            this.dataGridViewIntervalosMixtoChicuadrado.ReadOnly = true;
            this.dataGridViewIntervalosMixtoChicuadrado.Size = new System.Drawing.Size(402, 352);
            this.dataGridViewIntervalosMixtoChicuadrado.TabIndex = 36;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "orden";
            this.dataGridViewTextBoxColumn1.HeaderText = "Intervalo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "limiteInferior";
            this.dataGridViewTextBoxColumn2.HeaderText = "Límite Inferior";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 75;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "limiteSuperior";
            this.dataGridViewTextBoxColumn3.HeaderText = "Límite Superior";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 75;
            // 
            // frecuenciaObservada
            // 
            this.frecuenciaObservada.DataPropertyName = "frecuenciaObservada";
            this.frecuenciaObservada.HeaderText = "Frecuencia Observada";
            this.frecuenciaObservada.Name = "frecuenciaObservada";
            this.frecuenciaObservada.ReadOnly = true;
            this.frecuenciaObservada.Width = 75;
            // 
            // frecuenciaEsperada
            // 
            this.frecuenciaEsperada.DataPropertyName = "frecuenciaEsperada";
            this.frecuenciaEsperada.HeaderText = "Frecuencia Esperada";
            this.frecuenciaEsperada.Name = "frecuenciaEsperada";
            this.frecuenciaEsperada.ReadOnly = true;
            this.frecuenciaEsperada.Width = 75;
            // 
            // buttonGenerarChiCuadrado
            // 
            this.buttonGenerarChiCuadrado.Enabled = false;
            this.buttonGenerarChiCuadrado.Location = new System.Drawing.Point(806, 295);
            this.buttonGenerarChiCuadrado.Name = "buttonGenerarChiCuadrado";
            this.buttonGenerarChiCuadrado.Size = new System.Drawing.Size(76, 23);
            this.buttonGenerarChiCuadrado.TabIndex = 35;
            this.buttonGenerarChiCuadrado.Text = "Calcular!!!";
            this.buttonGenerarChiCuadrado.UseVisualStyleBackColor = true;
            this.buttonGenerarChiCuadrado.Click += new System.EventHandler(this.buttonGenerarChiCuadrado_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.inputMuestra);
            this.groupBox1.Controls.Add(this.cbMuestra);
            this.groupBox1.Location = new System.Drawing.Point(11, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(116, 83);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tamaño de N";
            // 
            // inputMuestra
            // 
            this.inputMuestra.Location = new System.Drawing.Point(4, 57);
            this.inputMuestra.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.inputMuestra.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputMuestra.Name = "inputMuestra";
            this.inputMuestra.Size = new System.Drawing.Size(106, 20);
            this.inputMuestra.TabIndex = 1;
            this.inputMuestra.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbMuestra
            // 
            this.cbMuestra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMuestra.FormattingEnabled = true;
            this.cbMuestra.Location = new System.Drawing.Point(4, 19);
            this.cbMuestra.Name = "cbMuestra";
            this.cbMuestra.Size = new System.Drawing.Size(106, 21);
            this.cbMuestra.TabIndex = 0;
            this.cbMuestra.SelectedIndexChanged += new System.EventHandler(this.cbMuestra_SelectedIndexChanged);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(54, 262);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 19;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // labelIntervalos
            // 
            this.labelIntervalos.AutoSize = true;
            this.labelIntervalos.Location = new System.Drawing.Point(484, 6);
            this.labelIntervalos.Name = "labelIntervalos";
            this.labelIntervalos.Size = new System.Drawing.Size(53, 13);
            this.labelIntervalos.TabIndex = 18;
            this.labelIntervalos.Text = "Intervalos";
            // 
            // labelNumerosGenerados
            // 
            this.labelNumerosGenerados.AutoSize = true;
            this.labelNumerosGenerados.Location = new System.Drawing.Point(133, 6);
            this.labelNumerosGenerados.Name = "labelNumerosGenerados";
            this.labelNumerosGenerados.Size = new System.Drawing.Size(104, 13);
            this.labelNumerosGenerados.TabIndex = 17;
            this.labelNumerosGenerados.Text = "Números Generados";
            // 
            // dataGridViewIntervalos
            // 
            this.dataGridViewIntervalos.AllowUserToAddRows = false;
            this.dataGridViewIntervalos.AllowUserToDeleteRows = false;
            this.dataGridViewIntervalos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIntervalos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.intervalo,
            this.limiteInferior,
            this.limiteSuperior,
            this.porcentajeMixto,
            this.porcentajeMultiplicativo,
            this.porcentajeAditivo,
            this.porcentajeLenguaje});
            this.dataGridViewIntervalos.Location = new System.Drawing.Point(487, 22);
            this.dataGridViewIntervalos.Name = "dataGridViewIntervalos";
            this.dataGridViewIntervalos.ReadOnly = true;
            this.dataGridViewIntervalos.Size = new System.Drawing.Size(855, 252);
            this.dataGridViewIntervalos.TabIndex = 16;
            // 
            // progressBarGenerandoRandoms
            // 
            this.progressBarGenerandoRandoms.Location = new System.Drawing.Point(13, 408);
            this.progressBarGenerandoRandoms.Maximum = 1;
            this.progressBarGenerandoRandoms.Name = "progressBarGenerandoRandoms";
            this.progressBarGenerandoRandoms.Size = new System.Drawing.Size(116, 23);
            this.progressBarGenerandoRandoms.Step = 1;
            this.progressBarGenerandoRandoms.TabIndex = 15;
            // 
            // intervalo
            // 
            this.intervalo.DataPropertyName = "orden";
            this.intervalo.HeaderText = "Intervalo";
            this.intervalo.Name = "intervalo";
            this.intervalo.ReadOnly = true;
            this.intervalo.Width = 50;
            // 
            // limiteInferior
            // 
            this.limiteInferior.DataPropertyName = "limiteInferior";
            this.limiteInferior.HeaderText = "Límite Inferior";
            this.limiteInferior.Name = "limiteInferior";
            this.limiteInferior.ReadOnly = true;
            this.limiteInferior.Width = 120;
            // 
            // limiteSuperior
            // 
            this.limiteSuperior.DataPropertyName = "limiteSuperior";
            this.limiteSuperior.HeaderText = "Límite Superior";
            this.limiteSuperior.Name = "limiteSuperior";
            this.limiteSuperior.ReadOnly = true;
            this.limiteSuperior.Width = 120;
            // 
            // porcentajeMixto
            // 
            this.porcentajeMixto.DataPropertyName = "porcentajeMixto";
            dataGridViewCellStyle5.Format = "00.00%";
            dataGridViewCellStyle5.NullValue = null;
            this.porcentajeMixto.DefaultCellStyle = dataGridViewCellStyle5;
            this.porcentajeMixto.HeaderText = "Porcentaje Mixto";
            this.porcentajeMixto.Name = "porcentajeMixto";
            this.porcentajeMixto.ReadOnly = true;
            this.porcentajeMixto.Width = 120;
            // 
            // porcentajeMultiplicativo
            // 
            this.porcentajeMultiplicativo.DataPropertyName = "porcentajeMultiplicativo";
            dataGridViewCellStyle6.Format = "00.00%";
            dataGridViewCellStyle6.NullValue = null;
            this.porcentajeMultiplicativo.DefaultCellStyle = dataGridViewCellStyle6;
            this.porcentajeMultiplicativo.HeaderText = "Porcentaje Multiplicativo";
            this.porcentajeMultiplicativo.Name = "porcentajeMultiplicativo";
            this.porcentajeMultiplicativo.ReadOnly = true;
            this.porcentajeMultiplicativo.Width = 150;
            // 
            // porcentajeAditivo
            // 
            this.porcentajeAditivo.DataPropertyName = "porcentajeAditivo";
            dataGridViewCellStyle7.Format = "00.00%";
            dataGridViewCellStyle7.NullValue = null;
            this.porcentajeAditivo.DefaultCellStyle = dataGridViewCellStyle7;
            this.porcentajeAditivo.HeaderText = "Porcentaje Aditivo";
            this.porcentajeAditivo.Name = "porcentajeAditivo";
            this.porcentajeAditivo.ReadOnly = true;
            this.porcentajeAditivo.Width = 120;
            // 
            // porcentajeLenguaje
            // 
            this.porcentajeLenguaje.DataPropertyName = "porcentajeLenguaje";
            dataGridViewCellStyle8.Format = "00.00%";
            this.porcentajeLenguaje.DefaultCellStyle = dataGridViewCellStyle8;
            this.porcentajeLenguaje.HeaderText = "Porcentaje Lenguaje";
            this.porcentajeLenguaje.Name = "porcentajeLenguaje";
            this.porcentajeLenguaje.ReadOnly = true;
            this.porcentajeLenguaje.Width = 130;
            // 
            // orden
            // 
            this.orden.DataPropertyName = "orden";
            this.orden.HeaderText = "Orden";
            this.orden.Name = "orden";
            this.orden.ReadOnly = true;
            this.orden.Width = 45;
            // 
            // columnMixto
            // 
            this.columnMixto.DataPropertyName = "randomMixto";
            this.columnMixto.HeaderText = "Mixto";
            this.columnMixto.Name = "columnMixto";
            this.columnMixto.ReadOnly = true;
            this.columnMixto.Width = 50;
            // 
            // columnMultiplicativo
            // 
            this.columnMultiplicativo.DataPropertyName = "randomMultiplicativo";
            this.columnMultiplicativo.HeaderText = "Multiplicativo";
            this.columnMultiplicativo.Name = "columnMultiplicativo";
            this.columnMultiplicativo.ReadOnly = true;
            this.columnMultiplicativo.Width = 75;
            // 
            // Aditivo
            // 
            this.Aditivo.DataPropertyName = "randomAditivo";
            this.Aditivo.HeaderText = "Aditivo";
            this.Aditivo.Name = "Aditivo";
            this.Aditivo.ReadOnly = true;
            this.Aditivo.Width = 50;
            // 
            // lenguaje
            // 
            this.lenguaje.DataPropertyName = "randomLenguaje";
            this.lenguaje.HeaderText = "Lenguaje";
            this.lenguaje.Name = "lenguaje";
            this.lenguaje.ReadOnly = true;
            this.lenguaje.Width = 60;
            // 
            // FormTpUno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1361, 730);
            this.Controls.Add(this.tabControlProgramas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximumSize = new System.Drawing.Size(1377, 769);
            this.MinimumSize = new System.Drawing.Size(1364, 726);
            this.Name = "FormTpUno";
            this.Text = "SIM - 4K4 - GRUPO P - TP1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNumeros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputSemilla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputConstanteIndependiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputConstanteMultiplicativa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputModulo)).EndInit();
            this.tabControlProgramas.ResumeLayout(false);
            this.tabIntegrantes.ResumeLayout(false);
            this.tabIntegrantes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabGeneradorAleatorios.ResumeLayout(false);
            this.tabGeneradorAleatorios.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputGradosLibertad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputNivelSignificancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputChiCuadradoTabulado)).EndInit();
            this.panelGrafico.ResumeLayout(false);
            this.panelGrafico.PerformLayout();
            this.panelContenedorGrafico.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartChiCuadrado)).EndInit();
            this.groupBoxCantidadIntervalos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inputCantidadIntervalos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputChiCuadradoCalculado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIntervalosMixtoChicuadrado)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inputMuestra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIntervalos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonGenerar;
        private System.Windows.Forms.DataGridView dataGridViewNumeros;
        private System.Windows.Forms.Label labelSemilla;
        private System.Windows.Forms.NumericUpDown inputSemilla;
        private System.Windows.Forms.NumericUpDown inputConstanteIndependiente;
        private System.Windows.Forms.Label labelConstanteIndependiente;
        private System.Windows.Forms.NumericUpDown inputConstanteMultiplicativa;
        private System.Windows.Forms.Label labelConstanteMultiplicativa;
        private System.Windows.Forms.NumericUpDown inputModulo;
        private System.Windows.Forms.Label labelModulo;
        private System.Windows.Forms.Button buttonAvanzarUno;
        private System.Windows.Forms.Button buttonAvanzarVeinte;
        private System.Windows.Forms.Button buttonAvanzarDiezmil;
        private System.Windows.Forms.TabControl tabControlProgramas;
        private System.Windows.Forms.TabPage tabGeneradorAleatorios;
        private System.Windows.Forms.ProgressBar progressBarGenerandoRandoms;
        private System.Windows.Forms.Label labelIntervalos;
        private System.Windows.Forms.Label labelNumerosGenerados;
        private System.Windows.Forms.DataGridView dataGridViewIntervalos;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.TabPage tabIntegrantes;
        private System.Windows.Forms.Label labelPozzoMarcelo;
        private System.Windows.Forms.Label labelIntegrantes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown inputMuestra;
        private System.Windows.Forms.ComboBox cbMuestra;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelFrecuenciaObservada;
        private System.Windows.Forms.Label labelFrecuenciaEsperada;
        private System.Windows.Forms.Panel panelGrafico;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartChiCuadrado;
        private System.Windows.Forms.ProgressBar progressBarChiCuadrado;
        private System.Windows.Forms.Label labelChiCuadradoCalculadoMixto;
        private System.Windows.Forms.NumericUpDown inputChiCuadradoCalculado;
        private System.Windows.Forms.DataGridView dataGridViewIntervalosMixtoChicuadrado;
        private System.Windows.Forms.Button buttonGenerarChiCuadrado;
        private System.Windows.Forms.Button buttonAutocalcularIntervalos;
        private System.Windows.Forms.NumericUpDown inputCantidadIntervalos;
        private System.Windows.Forms.GroupBox groupBoxCantidadIntervalos;
        private System.Windows.Forms.Label labelMetodo;
        private System.Windows.Forms.ComboBox comboMetodoChiCuadrado;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown inputChiCuadradoTabulado;
        private System.Windows.Forms.Button buttonCerrarGrafico;
        private System.Windows.Forms.Panel panelContenedorGrafico;
        private System.Windows.Forms.Button buttonMostrarGrafico;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown inputGradosLibertad;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown inputNivelSignificancia;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn frecuenciaObservada;
        private System.Windows.Forms.DataGridViewTextBoxColumn frecuenciaEsperada;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxTestChi;
        private System.Windows.Forms.DataGridViewTextBoxColumn intervalo;
        private System.Windows.Forms.DataGridViewTextBoxColumn limiteInferior;
        private System.Windows.Forms.DataGridViewTextBoxColumn limiteSuperior;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentajeMixto;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentajeMultiplicativo;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentajeAditivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentajeLenguaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn orden;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnMixto;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnMultiplicativo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aditivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn lenguaje;
    }
}

