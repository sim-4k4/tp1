﻿using MathNet.Numerics.Distributions;
using SIM_TP1.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace SIM_TP1.Utils
{
    internal class UtilsRandom
    {
        /// <summary>
        /// Devuelve una semilla basada en la formula de congruente mixto para generar numeros pseudoaleatorios.
        /// </summary>
        /// <param name="a">Constante multiplicativa</param>
        /// <param name="c">Constante independiente</param>
        /// <param name="m">Modulo</param>
        /// <param name="x">Semilla</param>
        /// <returns>Semilla congruente mixta redondeada en 4 decimales</returns>
        public static decimal getXiMixto(int a, int c, int m, decimal x)
        {
            return (a * x + c) % m;
        }

        /// <summary>
        /// Devuelve una semilla basada en la formula de congruente multiplicativo para generar numeros pseudoaleatorios.
        /// </summary>
        /// <param name="a">Constante multiplicativa</param>
        /// <param name="m">Modulo</param>
        /// <param name="x">Semilla</param>
        /// <returns>Semilla congruente multiplicativa redondeada en 4 decimales</returns>
        public static decimal getXiMultiplicativo(int a, int m, decimal x)
        {
            return (a * x) % m;
        }

        /// <summary>
        /// Devuelve una semilla basada en la formula de congruente aditivo para generar numeros pseudoaleatorios.
        /// </summary>
        /// <param name="m">Modulo</param>
        /// <param name="x">Semilla</param>
        /// <param name="xa">Semilla anterior</param>
        /// <returns>Semilla congruente aditiva redondeada en 4 decimales</returns>
        public static decimal getXiAditivo(int m, decimal x, decimal xa)
        {
            return (x + xa) % m;
        }

        /// <summary>
        /// Devuelve un numero aleatorio en base a una semilla
        /// </summary>
        /// <param name="x">Semilla</param>
        /// <param name="m">Modulo</param>
        /// <returns></returns>
        public static decimal getRandom(decimal x, int m)
        {
            return Decimal.Round(x / m, 4);
        }

        /// <summary>
        /// Dada una serie de numeros aleatorios y la cantidad de intervalos, realiza el test de chi cuadrado para esa serie.
        /// Agrega al chart pasado por parametros los puntos de frecuencia esperada y observada
        /// Agrega al bindingList los intervalos calculados.
        /// Devuelve el chi cuadrado calculado
        /// </summary>
        /// <param name="randoms">La serie de numeros aleatorios</param>
        /// <param name="cantidadIntervalos">La cantidad de intervalos</param>
        /// <param name="chart">El grafico de barras a graficar</param>
        /// <param name="lista">La lista bindeada a la grilla de intervalos</param>
        /// <param name="progressBar">El progress bar a actualizar</param>
        /// <returns></returns>
        public static decimal calcularChiCuadrado(List<decimal> randoms, decimal cantidadIntervalos, Chart chart, BindingList<FilaChiCuadrado> lista, ProgressBar progressBar)
        {
            randoms.Sort();

            decimal minimo = randoms.First();
            decimal maximo = randoms.Last();
            decimal rango = maximo - minimo;
            decimal amplitud = Math.Round(rango / cantidadIntervalos, 4);
            decimal frecuenciaEsperada = randoms.Count / cantidadIntervalos;

            for (int i = 0; i < cantidadIntervalos; i++)
            {
                int orden = i + 1;
                var tupla = Util.getNuevoIntervalo(i, minimo, amplitud);

                decimal limiteInferior = tupla.Item1;
                decimal limiteSuperior = tupla.Item2;

                int frecuenciaObservada;
                    
                if (i == cantidadIntervalos - 1)
                {
                    frecuenciaObservada = randoms.Count();
                } else
                {
                    frecuenciaObservada = randoms.FindIndex(r => r >= limiteSuperior) - 1;
                }

                if (frecuenciaObservada < 0)
                {
                    frecuenciaObservada = 0;
                }

                randoms.RemoveRange(0, frecuenciaObservada);

                decimal chiCuadradoCalculado = Math.Round((decimal)Math.Pow((double)frecuenciaEsperada - (double)frecuenciaObservada, 2) / frecuenciaEsperada, 4);

                FilaChiCuadrado fila = new FilaChiCuadrado();
                fila.limiteInferior = limiteInferior;
                fila.limiteSuperior = limiteSuperior;
                fila.frecuenciaObservada = frecuenciaObservada;
                fila.frecuenciaEsperada = frecuenciaEsperada;
                fila.chiCuadradoCalculado = chiCuadradoCalculado;
                fila.orden = orden;

                chart.Series["Frecuencia Observada"].Points.AddXY(orden, frecuenciaObservada);
                chart.Series["Frecuencia Esperada"].Points.AddXY(orden, frecuenciaEsperada);

                lista.Add(fila);

                progressBar.PerformStep();
            }

            return lista.Sum(fila => fila.chiCuadradoCalculado);
        }
        /// <summary>
        /// Obtener Chi cuadrado tabulado
        /// </summary>
        /// <param name="cantidad de intervalos"></param>
        /// <param name="nivel de significancia alfa"></param>
        /// <returns></returns>
        public static decimal getChiTabulado(int cantIntervs, decimal significancia_alfa)
        {
            int freedom = cantIntervs - 1;

            if (freedom < 1)
            {
                freedom = 1;
            }

            ChiSquared ch = new ChiSquared(freedom);
            return Util.Redondear4Decimales((decimal)ch.InverseCumulativeDistribution((double)(1 - significancia_alfa)));
        }

        /// <summary>
        /// Retorna la conclusión final luego de analizar el valor calculado de Chi respecto del obtenido por tabla para un nivel de significancia y grados de libertad dados 
        /// </summary>
        /// <param name="chi_cuadrado_calculado"></param>
        /// <param name="chi_tabulado"></param>
        /// <returns></returns>
        public static string getRespuestaFinal(decimal chi_cuadrado_calculado, decimal chi_tabulado)
        {
            if (chi_cuadrado_calculado <= chi_tabulado)
            {
                return "Dado que el chi calculado es menor o igual al chi por tabla NO se puede rechazar la hipótesis"
                    + " de que la muestra proviene de una distribución uniforme de probabilidad.";
            }

            return "Dado que el chi calculado es mayor al chi por tabla se puede rechazar la hipótesis"
                + " de que la muestra proviene de una distribución uniforme de probabilidad.";

        }
    }
}
