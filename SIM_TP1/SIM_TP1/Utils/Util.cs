﻿using SIM_TP1.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIM_TP1.Utils
{
    internal class Util
    {

        /// <summary>
        /// Devuelve un nuevo intervalo con los porcentajes inicializados en 0,
        /// cuyo limite inferior es el 'indice' dividido la cantidad de intervalos,
        /// y su limite superior es el limite inferior mas la amplitud
        /// </summary>
        /// <param name="indice"></param>
        /// <param name="cantidadIntervalos"></param>
        /// <param name="amplitud"></param>
        /// <returns>Un nuevo intervalo (limiteInferior, limiteSuperior)</returns>
        public static Tuple<decimal, decimal> getNuevoIntervalo(decimal indice, decimal minimo, decimal amplitud)
        {
            decimal limiteInferior = minimo + (amplitud * indice);
            decimal limiteSuperior = limiteInferior + amplitud;

            return new Tuple<decimal, decimal>(limiteInferior, limiteSuperior);
        }

        /// <summary>
        /// Devuelve el valor decimal ingresado por parámetro con cuatro decimales
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static decimal Redondear4Decimales(decimal valor)
        {
            decimal valorRedondeado = (decimal)Math.Round(valor, 4, MidpointRounding.AwayFromZero);
            return valorRedondeado;
        }

        /// <summary>
        /// Devuelve el valor decimal ingresado por parámetro con la cantidad de decimales que recibe por parámetro
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="cantidadDecimales"></param>
        /// <returns></returns>
        public static decimal RedondearDecimales(decimal valor, int cantidadDecimales)
        {
            decimal valorRedondeado = (decimal)Math.Round(valor, cantidadDecimales, MidpointRounding.AwayFromZero);
            return valorRedondeado;
        }
    }
}
